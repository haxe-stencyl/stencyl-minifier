import haxe.macro.Compiler;

class Minifier
{
	#if macro
	public static function addKeeps()
	{
		haxe.macro.Compiler.keep("scripts");
		haxe.macro.Compiler.keep("com.stencyl.models.Actor");
		haxe.macro.Compiler.keep("com.stencyl.behavior.ActorScript");	

	}
	#end
}
