

This extension is a convenience extension for Stencyl users, it just adds specific flags, which enables DCE, Dead Code Elimination.

It removes unused code, which saves place.

Doesn't work well for html5.



Tested on Stencyl 10500

.

 ### Examples

|                  |                  | Without DCE | With DCE   |
| ---------------- | ---------------- | ----------- | ---------- |
| Blank Game       | Flash            | 807k        | 506k       |
| Blank Game       | Linux            | 28.5 Mo     | 16,7 Mo    |
| Real Life Game 1 | Linux            | 43 Mo       | 33 Mo      |
| Real Life Game 2 | Android          | 59.482 KB   | 57.689 KB  |
| Real Life Game 2 | Windows( zipped) | 132.646 KB  | 131.270 KB |
| Real Life Game 2 | Mac              | 37.3 Mo     | 32 Mo      |
| Real Life Game 2 | iOS Simulator    | 21Mo        | 16 Mo      |
|                  |                  |             |            |

## Where can I know more about DCE ?



https://haxe.org/manual/cr-dce.html



## It doesn't compile after activating the extension ...

If it because of your own code, you can put  after the imports

```haxe
@:keep
```

You can add also haxe flags

```
--macro keep('type dot path')
```

If it is a normal Stencyl game without extensions, don't hesitate to contact me, I'll adapt the extension for you.

## And what about html5 ?



You can upload the file  of your game, for example BlankGame.js to

https://jscompress.com/



For a 3mb blank game, you can have 46.43% compression, saving 1354.9 kb  .



## Where can I discuss about the extension ?



You can discuss about the extension here

http://community.stencyl.com/index.php/topic,57491.0.html



## Changelog



Version 1.1 : updated to Stencyl 10500 and simplified the code.